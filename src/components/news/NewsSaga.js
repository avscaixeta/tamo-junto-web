import { put, call } from 'redux-saga/effects';
import axios from 'axios';

const URL = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/rest/v1/news' : 'https://tamojuntoweb.herokuapp.com/rest/v1/news';

console.log('Ambiente atual: ', process.env.NODE_ENV)

export function fetchAllNewsFromApi() {
    return axios.get(`${URL}`)
}

export function* fetchAllNews() {
    try {
        const response = yield call(fetchAllNewsFromApi);
        yield put({ type: 'SUCCESS_FIND_ALL', payload: {data: response } })
    } catch (err){
        yield put({ type: 'FAILURE_FIND_ALL' })
    }
}

export function* saveNewUrl(body) {
    try {
        yield call(axios.post, URL, body.data, 
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });
        yield put({ type: 'SUCCESS_CREATE_NEWS' })
    } catch (err) {
        yield put({ type: 'FAILURE_CREATE_NEWS' })
    }
}

export function* deleteUrl(body) {
    console.log(body.id)
    try {
        
        
        yield call(
            axios.delete, URL, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                params: 
                    {id:body.id}
                },
            
        );
        yield put({ type: 'SUCCESS_DELETE_NEWS' })
    } catch (err) {
        console.log('Erro ao deletar o registro: ', err)
        yield put({ type: 'FAILURE_DELETE_NEWS' })
    }
}