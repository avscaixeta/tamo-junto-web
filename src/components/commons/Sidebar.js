import React, { Component } from 'react';
// import SingleAccordion from './SingleAccordion';
import { Link } from "react-router-dom";

class Sidebar extends Component {
    render() {
        return (
            <div className={
                (this.props.className || '')
                + " adap-sidebar adap-light-gray edb-sidebar edb-bar-block edb-collapse2 edb-card"}>

                {/* <SingleAccordion header={
                    <div className="edb-bar-item edb-button">
                        <img src="/images/ico-agenda.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                        <span className="adap-sidebar-item-desc">
                            Agenda
                            <i className="fas fa-sort-down edb-small"></i>
                        </span>
                    </div>
                } content={
                    <div className="adap-sub-itens">
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-plus edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Novo evento</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-forward edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Próximos eventos</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-calendar-alt edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Agenda completa</span>
                        </a>
                    </div>
                } /> */}

                <div className="edb-bar-item edb-button">
                    <img src="/images/ico-arquivo.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                    <Link to="/home">
                        <span className="adap-sidebar-item-desc">Home</span>
                    </Link>	
                </div>
                <div className="edb-bar-item edb-button">
                    <img src="/images/ico-arquivo.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                    <Link to="/news">
                        <span className="adap-sidebar-item-desc">Notícias</span>
                    </Link>	
                </div >
                <div className="edb-bar-item edb-button">
                    <img src="/images/ico-arquivo.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                    <Link to="/campaign">
                        <span className="adap-sidebar-item-desc">Campanha</span>
                    </Link>	
                </div>


                {/* <a href="#menu-item" className="edb-bar-item edb-button">
                    <img src="/images/ico-alerta.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                    <Link to="/coord">
                        <span className="adap-sidebar-item-desc">
                            Avisos            
                        </span>
                    </Link>	
                    
                    <span className="adap-badge edb-dark-gray  edb-margin-small-left">99</span>
                </a> */}
                
                {/* <SingleAccordion header={
                    <div className="edb-bar-item edb-button">
                        <img src="/images/ico-livro.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                        <span className="adap-sidebar-item-desc">
                            Livros
                            <i className="fas fa-sort-down edb-small"></i>
                        </span>
                    </div>
                } content={
                    <div className="adap-sub-itens">
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-star edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Novidades</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-clock edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Livros recentes</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-graduation-cap edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Didático</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-book edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Literatura</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-newspaper edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Boletim Salesiano</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-bible edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Espiritualidade</span>
                        </a>
                    </div>
                } />

                <SingleAccordion header={
                    <div className="edb-bar-item edb-button">
                        <img src="/images/ico-lapis.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                        <span className="adap-sidebar-item-desc">
                            Exercícios
                            <i className="fas fa-sort-down edb-small"></i>
                        </span>
                    </div>
                } content={
                    <div className="adap-sub-itens">
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-calendar-alt edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Avaliações agendadas</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-book-reader edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Adaptativa+</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-check-square edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Acompanhamento pedagógico</span>
                        </a>
                    </div>
                } />

                <SingleAccordion header={
                    <div className="edb-bar-item edb-button">
                        <img src="/images/ico-rede.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                        <span className="adap-sidebar-item-desc">
                            Rede
                            <i className="fas fa-sort-down edb-small"></i>
                        </span>
                    </div>
                } content={
                    <div className="adap-sub-itens">
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-users edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Amigos</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-plug edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Conexões</span>
                        </a>
                    </div>
                } />

                <SingleAccordion header={
                    <div className="edb-bar-item edb-button">
                        <img src="/images/ico-compras.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                        <span className="adap-sidebar-item-desc">
                            Compras
                            <i className="fas fa-sort-down edb-small"></i>
                        </span>
                    </div>
                } content={
                    <div className="adap-sub-itens">
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-cart-plus edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Ativar compra</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-shopping-bag edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Loja</span>
                        </a>
                        <a href="#menu-item" className="edb-bar-item edb-button adap-gray ">
                            <i className="fas fa-file-invoice-dollar edb-large edb-margin-small-left"></i>
                            <span className="edb-margin-small-left">Portal e consultas</span>
                        </a>
                    </div>
                } /> */}

                {/* <a href="#menu-item" className="edb-bar-item edb-button">
                    <img src="/images/ico-lixeira.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                    
                    <Link to="/news">
                        <span className="adap-sidebar-item-desc">Lixeira geral</span>
                    </Link>	
                </a> */}
                <a href="#menu-item" className="edb-bar-item edb-button">
                    <img src="/images/ico-config.svg" width="26" alt="Ícone" className="edb-margin-small-right" />
                    <span className="adap-sidebar-item-desc">Configuração</span>
                </a>
            </div>
        );
    }
}

export default Sidebar;