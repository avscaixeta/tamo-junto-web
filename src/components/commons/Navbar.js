import React, { Component } from 'react';
import UserProfile from './UserProfile';

class Navbar extends Component {
  render() {
    return (     
        <div className="edb-theme edb-display-container edb-top edb-select-none">
        
          {/* Ícone menu */}
          <i alt="menu" className="fas fa-bars edb-cell-middle edb-xxlarge edb-margin-left" onClick={this.props.onClickMenuIcon}></i>

          {/* Logo edebê */}
          <img className="edb-margin" src="/images/logo-edebe.svg" alt="Tamo Junto App" width="78"></img>

          {/* Parte direita da Navbar */}
          <div className="edb-display-right edb-padding">
            <UserProfile className="edb-inline" />
            <div className="edb-inline edb-margin-left">
              <img src="/images/sino-notificacao.svg" width="28" alt="Notificações"/>
              <span className="adap-badge adap-notification-badge">99</span>
            </div>
          </div>
        </div>
    );
  }
}

export default Navbar;