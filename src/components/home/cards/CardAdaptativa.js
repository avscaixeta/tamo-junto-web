import React, { Component } from 'react';
import Rate from 'rc-rate';
import '../../../styles/rc-rate.css';

import DashBoardChartD3 from '../../charts/DashboardChartD3';
import ProgressBar from '../../commons/ProgressBar';
import ClampText from '../../../utility/ClampText';

class CardAdaptativa extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showEmptyState: false,
            trilhas:{
                trilha1: 100,
                trilha2: 30,
                trilha3: 0
            },
            notas:{
                notaTri: 760,
                pontos: 120,
                colocacao: 875
            },
            estudos:[
                { img: "/images/estudos/estudo1.png", titulo: "Globalização: da produção, do consumo", porcentagem: 90, qtd: 5, tipo: 'livro', pontos: 100, dia: 24, diaSemana: 'QUA' },
                { img: "/images/estudos/estudo2.png", titulo: "Calor, ambiente e usos de energia", porcentagem: 0, qtd: 5, tipo: 'video', pontos: 50, dia: 27, diaSemana: 'SEX'},
                { img: "/images/estudos/estudo3.png", titulo: "Calor, ambiente e usos de energia", porcentagem: 0, qtd: 5, tipo: 'questoes', pontos: 150, dia: 28, diaSemana: 'SAB'},
            ]
        };
    }
    getDescricaoTipo(tipo){
        if(tipo === 'livro') return 'páginas';
        if(tipo === 'video') return 'minutos';
        if(tipo === 'questoes') return 'questões';
    }
    getIconeTipo(tipo){
        if(tipo === 'livro') return (<i className="fas fa-book-reader"></i>);
        if(tipo === 'video') return (<i className="fas fa-play-circle"></i>);
        if(tipo === 'questoes') return (<i className="fas fa-pencil-alt"></i>);
    }

    render() {
        return (
            <div className="edb-card adap-card adap-card-adaptativa edb-topbar adap-border-green">
           

                <header className="edb-container edb-margin-small-top">
                    <span className="edb-large edb-text-bolder edb-hide-small edb-margin-small-right ">ADAPTATIVA |</span>
                    <span className="edb-text-bold">PRÓXIMOS ESTUDOS</span>
                    <div className="edb-display-right edb-margin-right">
                         <Rate count="3" character={<i className="fas fa-star"></i>}/>
                        <i className="fas fa-ellipsis-h edb-margin-left"></i>
                    </div>
                </header>
                {this.state.showEmptyState &&
                    <div className="edb-container edb-padding-bottom">
                        <div className="edb-row edb-padding-top">

                            <div className="edb-container edb-col s8">
                                <span className="edb-large edb-text-bold">
                                    O que é autoaprendizado? É possível? O que ele come, aonde vive?
                            </span>
                                <p>
                                    Não espere a TV te mostrar isso. Faça o simulado de diagnóstico e comece essa incrível jornada para seu sucesso.
                            </p>
                                <button className="edb-button adap-green">Iniciar simulado diagnóstico</button>
                            </div>
                            <div className="edb-col s4">
                                <img className="edb-image" src="/images/professor-televisao.svg" alt="Imagem professor numa televisão" />
                            </div>
                        </div>
                    </div>}
                {!this.state.showEmptyState &&
                    <div>
                        <div className="edb-padding-bottom edb-hide-medium edb-hide-large">
                            {this.state.estudos.map(item => 
                                <div className="edb-row-cell edb-margin-small-top">
                                    <div className="edb-cell">
                                        <div className=" adap-agenda-dia">
                                            <div className="edb-tiny">{item.diaSemana}</div>
                                            <div className="edb-large edb-text-bolder edb-line-height-small">{item.dia}</div>
                                        </div>
                                    </div>
                                    <div className="edb-cell edb-xlarge adap-text-green edb-container">
                                        {this.getIconeTipo(item.tipo)}
                                    </div>
                                    <div className="edb-cell  edb-padding-right">
                                        <div className="edb-text-bold edb-line-height-small">{item.titulo}</div>
                                        <span className="edb-small">{item.qtd} {this.getDescricaoTipo(item.tipo)}, {item.pontos} pontos</span>
                                    </div>
                                </div>
                            )}

                            <div className="edb-center edb-margin-top">
                                <i className="fas fa-plus"></i> <span className="edb-small"> Carregar mais</span>
                            </div>
                        </div>
                        <div className="edb-container edb-padding-bottom edb-hide-small">

                            <div className="adap-scroll-horizontal edb-margin-small-top" >
                                {this.state.estudos.map(item => 
                                    <div className="edb-row-cell edb-inline edb-margin-right adap-light-gray">
                                        <div className="edb-cell">
                                            <img className="" src={item.img} alt={item.titulo} />
                                        </div>
                                        <div className="edb-cell ">
                                            <div className="edb-container adap-estudo-desc ">
                                                <div className="edb-text-bold edb-small">
                                                    {ClampText(item.titulo, 30)}
                                                </div>
                                                <div className="edb-tiny adap-text-green">{item.porcentagem}%</div>
                                                <div className="edb-tiny edb-text-bold">Tipos de conjuntos</div>
                                                <div className="edb-tiny"><span className="edb-text-bold">{item.qtd}</span> {this.getDescricaoTipo(item.tipo)}, <span className="edb-text-bold">{item.pontos}</span> pontos</div>
                                            </div>
                                        </div>
                                    </div>
                                )}

                            </div>
                            <div className="edb-row">
                                <div className="edb-col m12 l4">

                                    <ProgressBar className="edb-margin-small-top" label="TRILHA 1" percentage={this.state.trilhas.trilha1} />
                                    <ProgressBar className="edb-margin-small-top" label="TRILHA 2" percentage={this.state.trilhas.trilha2} />
                                    <ProgressBar className="edb-margin-small-top" label="TRILHA 3" percentage={this.state.trilhas.trilha3} />

                                    <div className="edb-row edb-center edb-margin-small-top">
                                        <div className="edb-col s4">
                                            <div className="edb-tiny">NOTA TRI.</div>
                                            <div className="edb-text-bold edb-large">{this.state.notas.notaTri}</div>
                                        </div>
                                        <div className="edb-col s4">
                                            <div className="edb-tiny">PONTOS</div>
                                            <div className="edb-text-bold edb-large">{this.state.notas.pontos}</div>
                                        </div>
                                        <div className="edb-col s4">
                                            <div className="edb-tiny">COLOCAÇÂO</div>
                                            <div className="edb-text-bold edb-large">{this.state.notas.colocacao}º</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="edb-col m12 l8">
                                    <DashBoardChartD3/>
                                </div>
                            </div>

                        </div>
                    </div>}
            </div>
        );
    }
}

export default CardAdaptativa;