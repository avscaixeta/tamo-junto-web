import { takeEvery, put, call, all } from 'redux-saga/effects';
import { fetchAllNews, saveNewUrl, deleteUrl } from '../src/components/news/NewsSaga';
import { fetchAllDocuments, saveNewDocument } from '../src/components/campaign/CampaignSaga';

export default function* rootSaga() {
    yield all([
        takeEvery('ASYNC_ADD_TODO', asyncAddTodo),
        
        //NEWS
        takeEvery('FIND_ALL_NEWS', fetchAllNews),
        takeEvery('SAVE_NEW_URL', saveNewUrl),
        takeEvery('DELETE_URL', deleteUrl),
        
        //CAMPAIGN
        takeEvery('FIND_ALL_DOCUMENTS', fetchAllDocuments),
        takeEvery('SAVE_NEW_DOCUMENT', saveNewDocument),
    ])
}

function* asyncAddTodo(action) {
    try {
        const response = yield call(fetchAllNews, action.payload.text);
        yield put({ type: 'ADD_TODO', payload: {text: response} })
    } catch (err){
        yield put({ type: 'ERROR' })
    }
}

