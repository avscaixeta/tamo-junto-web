import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga';
import NewsReducer from '../components/news/NewsReducer'

// Integrando com o redux-dev-tools do CHROME
const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  combineReducers({NewsReducer}),
  compose(applyMiddleware(sagaMiddleware), devTools),
);

sagaMiddleware.run(rootSaga);

export default store;