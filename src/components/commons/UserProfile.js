import React, { Component } from 'react';

class UserProfile extends Component {
    render() {
        return (
            <div className={this.props.className}>
                <div className="edb-hide-small edb-inline edb-cell-middle edb-lineheight-default edb-right-align">
                    <h6 className="edb-margin-none"><b>Laura Ferreira Nunes</b></h6>
                    <div className="edb-small">Candidato - Loren Ipsun</div>
                    <div className="edb-small">Araxa - MG</div>
                </div>


                <div className="edb-dropdown-hover">
                    <img className="edb-margin-small-left" src="/images/perfil.png" alt="Foto do perfil" />
                    <i className="fas fa-sort-down edb-small edb-margin-small-left"></i>
                    <div className="edb-dropdown-content edb-bar-block edb-border" style={{ right: 0 }}>
                        <a href="/" className="edb-bar-item edb-button  edb-small">Link 1</a>
                        <a href="/" className="edb-bar-item edb-button  edb-small">Link 2</a>
                        <a href="/" className="edb-bar-item edb-button  edb-small">Link 3</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserProfile;