
export function findAllDocuments() {
    return {
        type: 'FIND_ALL_DOCUMENTS',
    }
}

export function saveNewDocument(data) {
    return {
        type: 'SAVE_NEW_DOCUMENT',
        data
    }
}