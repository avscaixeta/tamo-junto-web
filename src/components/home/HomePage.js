
import React, { Component } from 'react';

import UserProfileHeader from '../commons/UserProfileHeader';
import CardAdaptativa from './cards/CardAdaptativa';
import CardAgenda from './cards/CardAgenda';
import CardAvaliacoesAgendadas from './cards/CardAvaliacoesAgendadas';
import CardLivrosRecentes from './cards/CardLivrosRecentes';

class HomePage extends Component {

  render() {
    return (
        <div>

          <UserProfileHeader className="edb-hide-medium edb-hide-large"/>

          <div className="edb-row edb-row-padding-large edb-row-padding-none-mobile ">

            <div className="edb-col s12 l7 edb-margin-large-bottom">
              <CardAdaptativa />
            </div>

            <div className="edb-col s12 l5 edb-margin-large-bottom">
              <CardAgenda />
            </div>
          </div>

          <div className="edb-row edb-row-padding-large edb-row-padding-none-mobile">

            <div className="edb-col s12 l6 edb-margin-large-bottom">
              <CardLivrosRecentes />
            </div>

            <div className="edb-col s12 l6 edb-margin-large-bottom">
              <CardAvaliacoesAgendadas />
            </div>
          </div>

        </div>
    );
  }
}

export default HomePage;
