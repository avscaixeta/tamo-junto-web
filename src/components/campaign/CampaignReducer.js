const initialState = {
    campaignData: null,
    data: [],
    create: false,
    error: false,
    successfullyCreatedNews: false,
}

export default function(state = initialState, action) {
    switch(action.type) {
        case 'FIND_ALL':
            return {
                ...state, 
                loading: true
            }
        case 'SUCCESS_FIND_ALL':
            return {
                data: action.payload.data,
                loading: false,
                error: false
            }
        case 'FAILURE_FIND_ALL':
            return {
                data: [],
                loading: false,
                error: true
            }   
        case 'SUCCESS_CREATE_DOCUMENT':
            return {
                successfullyCreatedNews: true,
                loading: false,
                error: false
            }
        case 'FAILURE_CREATE_DOCUMENT':
            return {
                successfullyCreatedNews: false,
                loading: false,
                error: true
            }        
        default:
            return state
    }

}