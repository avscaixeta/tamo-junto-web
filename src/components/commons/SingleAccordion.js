import React, { Component } from 'react';

class SingleAccordion extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen:  props.isOpen || false
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        return (
            <>
                <div onClick={this.handleClick}>{this.props.header}</div>
                <div className="edb-animate-opacity" style={{display: (this.state.isOpen) ? "block" : "none"}}>
                    {this.props.content}
                </div>
            </>
        );
    }
}

export default SingleAccordion;