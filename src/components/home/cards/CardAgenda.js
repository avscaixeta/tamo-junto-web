import React, { Component } from 'react';
import Rate from 'rc-rate';
import '../../../styles/rc-rate.css';

class CardAgenda extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showEmptyState: true,
            mesAgenda: 'janeiro',
            anoAgenda: '2019',
            semana: [
                { dia: 23, diaSemana: 'SEG' },
                { dia: 24, diaSemana: 'TER' },
                { dia: 25, diaSemana: 'QUA' },
                { dia: 26, diaSemana: 'QUI' },
                { dia: 27, diaSemana: 'SEX' },
                { dia: 28, diaSemana: 'SAB' },
                { dia: 29, diaSemana: 'DOM' }
            ],
            itensAgenda: [
                { horario: '08:00', descricao: 'Exemplo de registro na agenda.' },
                { horario: '09:18', descricao: 'Outro exemplo de registro na agenda.' }
            ]
        };
    }
    render() {
        return (
            <div className="edb-card adap-card edb-topbar adap-border-yellow">

                    <div className="">
                        <header className="edb-border-bottom adap-agenda-header">
                            <span className="edb-large edb-text-bolder">{this.state.mesAgenda.toUpperCase()} |</span>
                            <span className="edb-margin-small-left edb-text-bold">{this.state.anoAgenda}</span>
                            <div className="edb-display-right edb-margin-right">
                                <Rate count="3" character={<i className="fas fa-star"></i>} />
                                <i className="fas fa-ellipsis-h edb-margin-left"></i>
                            </div>
                        </header>

                        <div className="edb-row adap-agenda-container">
                        <div className="edb-col edb-border-right adap-agenda-dias ">
                        {this.state.semana.map(item =>
                            <div className=" adap-agenda-dia ">
                                <div className="edb-tiny">{item.diaSemana}</div>
                                <div className="edb-large edb-text-bolder edb-line-height-small">{item.dia}</div>
                            </div>
                        )}
                    </div>
                    <div className="edb-rest ">

                        {this.state.showEmptyState && <>
                            <div className="edb-padding-small">
                                <div className="edb-row edb-margin-small-top">
                                    <div className="edb-col s2 edb-small edb-text-bold edb-center">08:00</div>
                                    <div className="edb-col s10 edb-small">Esse é o calendário de atividades da sua escola. Você ainda não tem nada programado mas poderá acompanhar tudo por aqui.</div>
                                </div>
                                <div className="edb-row edb-margin-small-top">
                                    <div className="edb-col s2 edb-small edb-text-bold edb-center">09:18</div>
                                    <div className="edb-col s10 edb-small">Você pode também registrar aqui todos os seus compromissos. Começa agora a usar sua agenda. Adicione um evento</div>
                                </div>
                            </div>
                            <img className="adap-agenda-seta" src="/images/seta-agenda.svg" alt="Indicação para criar um novo evento" />
                        </>}
                        {!this.state.showEmptyState && <>
                            <div className="edb-padding-small">
                                {this.state.itensAgenda.map(item =>
                                    <div className="edb-row edb-margin-small-top">
                                        <div className="edb-col s2 edb-small edb-text-bold edb-center">{item.horario}</div>
                                        <div className="edb-col s10 edb-small">{item.descricao}</div>
                                    </div>
                                )}
                            </div>
                        </>}
                    </div>
                    </div>
                </div>
                <div className="edb-row">
                    <div className="edb-col s6">
                        <button className="edb-button edb-block adap-green">
                            <i className="fas fa-plus edb-large edb-margin-right"></i>
                            Novo evento
                        </button>
                    </div>
                    <div className="edb-col s6">
                        <button className="edb-button edb-block edb-blue">
                            <i className="fas fa-calendar-alt edb-large edb-margin-right"></i>
                            Agenda completa
                        </button>
                    </div>
                </div>

            </div>
        );
    }
}

export default CardAgenda;