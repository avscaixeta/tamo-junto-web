import React, { Component } from 'react';
import Rate from 'rc-rate';
import '../../../styles/rc-rate.css';
import ClampText from '../../../utility/ClampText';

class CardAvaliacoesAgendadas extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showEmptyState: false,
            itensTabela: [
                { 
                    titulo: 'Atividade 1º Ano 2ª Etapa Av3', 
                    materia: 'Matemática',
                    situacao: 'STATUS_REALIZADA',
                    nota: 5.7,
                    notaMax: 10,
                    data: { dia: '01', mes: 'nov' }
                },
                { 
                    titulo: 'Prova AV3 2º ano', 
                    materia: 'Português',
                    situacao: 'STATUS_CORRECAO',
                    nota: 7,
                    notaMax: 10,
                    data: { dia: '29', mes: 'out' }
                },
                { 
                    titulo: 'Prova Biologia Av1 1ª Etapa', 
                    materia: 'Biologia',
                    situacao: 'STATUS_EXPIRADA',
                    nota: 15,
                    notaMax: 15,
                    data: { dia: '05', mes: 'out' }
                }
            ],
            avaliacoes: [
                {
                    titulo: 'Atividade 1º Ano 2ª',
                    etapa: 'Etapa Av3',
                    materia: 'GEOGRAFIA',
                    materiaAbrev: 'GEO',
                    professor: 'Prof. Eline Ferreira de Medeiros',
                    icoMateria: "/images/materias/ico-geo.svg",
                    data: {
                        dia: '17',
                        diaSemana: 'QUA',
                        mes: 'NOV',
                        horario: '17:30'
                    },
                    data2: {
                        dia: '24',
                        diaSemana: 'SAB',
                        mes: 'NOV',
                        horario: '17:30'
                    }
                },
                {
                    titulo: 'Prova Av3',
                    etapa: '2º ano',
                    materia: 'FÍSICA',
                    materiaAbrev: 'FIS',
                    professor: 'Prof. Andrezza Lourdes Guimaraes Cacione',
                    icoMateria: "/images/materias/ico-fisica.svg",
                    data: {
                        dia: '20',
                        diaSemana: 'TER',
                        mes: 'NOV',
                        horario: '17:30'
                    },
                    data2: {
                        dia: '25',
                        diaSemana: 'DOM',
                        mes: 'NOV',
                        horario: '17:30'
                    }
                },
                {
                    titulo: 'Av1 Prova Matemática',
                    etapa: '1ª Etapa',
                    materia: 'MATEMÁTICA',
                    materiaAbrev: 'MAT',
                    professor: 'Prof. Jorge Luiz Farago',
                    icoMateria: "/images/materias/ico-matematica.svg",
                    data: {
                        dia: '28',
                        diaSemana: 'QUA',
                        mes: 'NOV',
                        horario: '8:00'
                    },
                    data2: {
                        dia: '30',
                        diaSemana: 'SEX',
                        mes: 'NOV',
                        horario: '18:00'
                    }
                },
            ]
        };
    }
    getTagSituacao(statusSituacao){
        if(statusSituacao === 'STATUS_REALIZADA') return (<span className="edb-tag tag-situacao edb-round adap-green">REALIZADA</span>);
        if(statusSituacao === 'STATUS_CORRECAO') return (<span className="edb-tag tag-situacao edb-round edb-yellow">CORREÇÂO</span>);
        if(statusSituacao === 'STATUS_EXPIRADA') return (<span className="edb-tag tag-situacao edb-round edb-red">EXPIRADA</span>);
    }
    render() {
        return (
            <div className="edb-card adap-card adap-card-avaliacoes edb-topbar adap-border-blue">
                <header className="edb-container edb-margin-small-top">
                    <span className="edb-large edb-text-bolder">AVALIAÇÕES AGENDADAS</span>
                    <div className="edb-display-right edb-margin-right">
                        <Rate count="3" character={<i className="fas fa-star"></i>} />

                        <div className="edb-dropdown-hover">
                            <i className="fas fa-ellipsis-h edb-margin-left"></i>
                            <div className="edb-dropdown-content edb-bar-block edb-border" style={{ right: 0 }}>
                                <a href="/" className="edb-bar-item edb-button edb-small">Avaliações agendadas</a>
                                <a href="/" className="edb-bar-item edb-button edb-small">Avaliações relizadas</a>
                                <a href="/" className="edb-bar-item edb-button edb-small">Avaliações expiradas</a>
                                <a href="/" className="edb-bar-item edb-button edb-small">Reportar erro</a>
                            </div>
                        </div>
                    </div>
                </header>

                {this.state.showEmptyState &&
                    <div className="edb-container edb-padding-bottom">
                        <div className="edb-cell-row edb-padding-top">

                            <div className="edb-container edb-cell edb-right-align">
                                <img src="/images/menina-mochila.svg" alt="Imagem menina de mochila nas costas" />
                            </div>
                            <div className="edb-cell edb-cell-bottom">
                                <i className="fas fa-comment edb-margin-small-right edb-xlarge"></i>
                                <span className="edb-text-bold">Professor, cadê?</span>
                                <p className="edb-small" style={{ maxWidth: "300px" }}>OK, você está ansioso(a) para ganhar pontos, demonstrar seus conhecimentos e evoluir cada dia mais. Então, pergunte a seu professor hoje mesmo: onde estão minhas avaliações?</p>
                            </div>
                        </div>
                    </div>}

                {!this.state.showEmptyState &&
                    <div>
                        <div className="edb-hide-medium edb-hide-large edb-padding-bottom">
                            {this.state.avaliacoes.map(item =>
                                <div className="edb-row edb-margin-small-top">
                                    <div className="edb-col s2 edb-center"><img src={item.icoMateria} alt={item.materia} /></div>
                                    <div className="edb-col s8">
                                        <div className="edb-small">{item.materia}</div>
                                        <div className="edb-small edb-text-bold">{item.titulo}</div>
                                    </div>
                                    <div className="edb-col s2">
                                        <div className="adap-agenda-dia edb-right edb-margin-small-right">
                                            <div className="edb-tiny">{item.data.diaSemana}</div>
                                            <div className="edb-large edb-text-bolder edb-line-height-small">{item.data.dia}</div>
                                        </div>
                                    </div>
                                </div>
                            )}
                            <div className="edb-center edb-margin-top">
                                <i className="fas fa-plus"></i> <span className="edb-small"> Carregar mais</span>
                            </div>
                        </div>
                        <div className="edb-container edb-padding-bottom edb-hide-small">

                            <div className="adap-scroll-horizontal edb-margin-small-top" >

                                {this.state.avaliacoes.map(item => 
                                
                                    <div className="adap-avaliacao-item edb-light-gray edb-padding edb-margin-right">
                                        <div className="edb-small edb-text-bold">
                                            {ClampText(item.titulo, 14)}
                                            <br />
                                            {item.etapa}
                                        </div>

                                        <div className="edb-row-cell  edb-margin-small-top">
                                            <div className="edb-cell">
                                                <div className="edb-small edb-center">
                                                    <img src={item.icoMateria} alt={item.materia} />
                                                    {item.materiaAbrev}
                                            </div>
                                            </div>
                                            <div className="edb-cell edb-cell-middle">
                                                <div className="edb-tiny">{ClampText(item.professor, 20)}</div>
                                            </div>
                                        </div>

                                        <div className="edb-row-cell edb-margin-small-top">
                                            <div className="edb-cell ">
                                                <div className="edb-small">{item.data.mes}</div>
                                                <div className="edb-xlarge edb-line-height-medium">{item.data.dia}</div>
                                                <div className="edb-small">{item.data.horario}</div>
                                            </div>
                                            <div className="edb-cell edb-cell-middle edb-padding-small">
                                                <i className="fas fa-arrow-right edb-xlarge "></i>
                                            </div>
                                            <div className="edb-cell edb-padding-small adap-data-inativa" >
                                                <div className="edb-small">{item.data2.mes}</div>
                                                <div className="edb-xlarge edb-line-height-medium">{item.data2.dia}</div>
                                                <div className="edb-small">{item.data2.horario}</div>
                                            </div>

                                        </div>
                                    </div>
                                 )}
                            </div>

                            <table className="edb-table  edb-tiny">
                                <thead>
                                    <tr>
                                        <th>DATA</th>
                                        <th>AVALIAÇÂO</th>
                                        <th>DISCIPLINA</th>
                                        <th>SITUAÇÂO</th>
                                        <th>NOTA</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.itensTabela.map(item => 
                                    <tr>
                                        <td>{item.data.dia}/{item.data.mes}</td>
                                        <td>{item.titulo}</td>
                                        <td>{item.materia}</td>
                                        <td> {this.getTagSituacao(item.situacao)}  </td>
                                        <td>{item.nota}/{item.notaMax}</td>
                                    </tr>
                                    )}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>}
            </div>
        );
    }
}

export default CardAvaliacoesAgendadas;