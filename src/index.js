import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";
import store from '../src/store'; 
import App from './components/App';
import { Provider } from 'react-redux';

import './styles/edb.css';
import './styles/adap.css';
import './styles/charts-d3.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import "antd/dist/antd.css";

ReactDOM.render(

    <Router>
        <Provider store={ store }>
            <App />
        </Provider>
    </Router>, 

    document.getElementById('root')
);
