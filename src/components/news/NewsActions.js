
export function findAll() {
    return {
        type: 'FIND_ALL_NEWS',
    }
}

export function saveNewUrl(data) {
    return {
        type: 'SAVE_NEW_URL',
        data
    }
}

export function deleteUrl(id) {
    return {
        type: 'DELETE_URL',
        id
    }
}

export function addTodo(text) {
    return {
        type: 'ASYNC_ADD_TODO',
        payload: {
            text,
        }
    }
}




