const initialState = {
    newsData: null,
    data: [],
    create: false,
    error: false,
    successfullyCreatedNews: false,
    successfullyDeletedNews: false,
}

export default function(state = initialState, action) {
    switch(action.type) {
        case 'FIND_ALL_NEWS':
            return {
                ...state, 
                loading: true
            }
        case 'SUCCESS_FIND_ALL':
            return {
                data: action.payload.data,
                loading: false,
                error: false
            }
        case 'FAILURE_FIND_ALL':
            return {
                data: [],
                loading: false,
                error: true
            }   
        case 'SUCCESS_CREATE_NEWS':
            return {
                successfullyCreatedNews: true,
                loading: false,
                error: false
            }
        case 'FAILURE_CREATE_NEWS':
            return {
                successfullyCreatedNews: false,
                loading: false,
                error: true
            }  
        case 'SUCCESS_DELETE_NEWS':
            return {
                successfullyDeletedNews: true,
                loading: false,
                error: false
            }
        case 'FAILURE_DELETE_NEWS':
            return {
                successfullyDeletedNews: false,
                loading: false,
                error: true
            }       
        case 'ADD_TODO':
            return {...state, value: action.payload}
        default:
            return state
    }

}