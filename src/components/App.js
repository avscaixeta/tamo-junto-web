
import React, { Component } from 'react';
import Navbar from './commons/Navbar';
import Sidebar from './commons/Sidebar';
import { BrowserRouter as Router, Route } from "react-router-dom";

import HomePage from './home/HomePage';
import CoordenadorPage from './coordenador/CoordenadorPage';
import NewsPage from './news/NewsPage';
import CampaignPage from './campaign/CampaignPage';

class App extends Component {

	constructor(props) {
		super(props);

		this.state = {
			isMenuOpen: true
		};

		this.onClickMenuIcon = this.onClickMenuIcon.bind(this);
		this.handleResize = this.handleResize.bind(this);

	}

	componentDidMount() {
		window.addEventListener('resize', this.handleResize)
		this.handleResize();
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.handleResize)
	}

	handleResize() {
		//Fechando o menu quando estiver numa resolução pequena (max-width: 993px)
		if (window.innerWidth < 993) {
			this.setState({ isMenuOpen: false });
		}
	}

	onClickMenuIcon() {
		this.setState({ isMenuOpen: !this.state.isMenuOpen });
	}

	render() {
		return (
			<div className={(!this.state.isMenuOpen ? "adap-sidebar-collapsed" : "")}>
				<Router>
					<div>
						<Navbar onClickMenuIcon={this.onClickMenuIcon} />
						<Sidebar />
						<div className="adap-sidebar-fix edb-top-fix adap-main-container edb-padding-large-top">
							<div>
								<Route exact path="/home" component={HomePage}/>
								<Route exact path="/coord" component={CoordenadorPage}/>
								<Route exact path="/news" component={NewsPage}/>
								<Route exact path="/campaign" component={CampaignPage}/>
							</div>
						</div>
					</div>
				</Router>
			</div>
		);
	}
}

export default App;
