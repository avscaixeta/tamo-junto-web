import React, { Component } from 'react';

class AdaptativaHeader extends Component {

    render() {
      return (
          <div className="adap-main-header">
              {/* <img className="adap-camaleao" src="/images/tamojunto.png" alt="Camaleão"/> */}
              <div className="adap-hero-title">Tamo Junto <i className="fas fa-plus-square"></i></div>
          </div>
      );
    }
}

export default AdaptativaHeader;