import React, { Component } from 'react';
// import { withRouter } from "react-router-dom";


// import {Button} from 'primereact/button';
// import { DataTable } from 'primereact/components/datatable/DataTable';
// import { Column } from 'primereact/components/column/Column';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';

import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import { addTodo, findAll, saveNewUrl, deleteUrl } from './NewsActions';
import AdaptativaHeader from '../commons/AdaptativaHeader';

import { Button, Icon, Popconfirm } from 'antd';

const mapStateToProps = state => ({
    data: state.NewsReducer.data,
    successfullyCreatedNews: state.NewsReducer.successfullyCreatedNews,
    successfullyDeletedNews: state.NewsReducer.successfullyDeletedNews,
});

const mapDispatchToProps = dispath => {
    return bindActionCreators({ addTodo, findAll, saveNewUrl, deleteUrl }, dispath);
};

export default connect(mapStateToProps, mapDispatchToProps)(
    class newsPage extends Component {

        constructor(props) {
            super(props);

            this.state = {
                isModalCreate: false,
                newsData: [],
                urlParam: '',
                brand: null,
                colors: null,
            };
            this.actionTemplate = this.actionTemplate.bind(this);
            this.onHideModalCreate = this.onHideModalCreate.bind(this);
            this.onHideModalDelete = this.onHideModalDelete.bind(this);

            this.props.findAll();
        }

        static getDerivedStateFromProps(nextProps, prevState) {
            let nextState = {};
            if (nextProps.successfullyCreatedNews) {
                nextProps.findAll();
            }
            if (nextProps.successfullyDeletedNews) {
                nextProps.findAll();
            }
            
            return nextState;
        }

        actionTemplate(rowData, column) {
            return <div>
                <Button onClick={(e) => this.setState({ isModalDelete: true })} type="danger">
                    <Icon type="delete" />
                </Button>
            </div>;
        }

        onHideModalCreate() {
            this.setState({ isModalCreate: false });
        }

        onHideModalDelete() {
            this.setState({ isModalDelete: false });
        }

        onShowModalDelete(record) {
            this.setState({ isModalDelete: true })
        }

        onSave = event => {
            event.preventDefault();
            this.props.saveNewUrl({ url: this.state.urlParam })
            this.onHideModalCreate();
        }

        onDelete = url => {
            console.log(url)
            this.props.deleteUrl(url.id)
        }

        render() {
            // console.log('NEWS DATA RESPONSE: ', this.props.data)

            const footerModalCreate = (
                <div>
                    <Button type="primary" label="Salvar" icon="pi pi-check" onClick={this.onSave}> Salvar </Button>
                    <Button type="primary" label="Cancelar" icon="pi pi-times" onClick={this.onHideModalCreate}> Cancelar </Button>
                </div>
            );

            const footerModalDelete = (
                <div>
                    <Button type="primary" label="Sim" icon="pi pi-check" onClick={this.onDelete}> Sim </Button>
                    <Button type="primary" label="Cancelar" icon="pi pi-times" onClick={this.onHideModalDelete}> Cancelar </Button>
                </div>
            );

            return (
                <div className="edb-padding-large-left edb-padding-large-right">
                    <AdaptativaHeader />
                    <div>
                        <div className="row">
                            <div className="col-lg-12" style={{ paddingTop: "20px" }}>
                                <span><h2>Gerenciamento das URL's</h2></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-right" style={{
                                "paddingBottom": "20px",
                                "paddingRight": "30px"}}>
                                <Button type="primary" onClick={(e) => this.setState({ isModalCreate: true })}>
                                    Adicionar URL
                                </Button>
                            </div>
                            <div className="col-lg-12 ">
                                <div className="main-content">
                                    <div className="container py-4 my-2">
                                        <div className="card border-0">
                                            <div className="card-body m-0 p-0">
                                                <table className="table bg-white table-striped table-bordered border-0 table-hover">
                                                    <thead style={{backgroundColor: "#628dcd"}}>
                                                        <tr>
                                                            <th className="text-center">URL</th>
                                                            <th className="text-center">Situação</th>
                                                            <th className="text-center">Data Cadastro</th>
                                                            <th className="text-center">Ações</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.props.data && this.props.data.data && this.props.data.data.data.map((record, i) => (
                                                            <tr key={i}>
                                                                <td className="v-align-middle text-center">
                                                                    <span>{record.url}</span>
                                                                </td>
                                                                <td className="v-align-middle text-center">
                                                                    <span>{record.status}</span>
                                                                </td>
                                                                <td className="v-align-middle text-center">
                                                                    <span>{record.createdAt}</span>
                                                                </td>
                                                                <td className="v-align-middle text-center">
                                                                    <Popconfirm title="Tem ceteza que deseja remover o item?"
                                                                                onConfirm={(e) => {this.onDelete(record)}}
                                                                                okText="Sim"
                                                                                cancelText="Cancelar">
                                                                        <span className="btn btn-danger btn-sm"> <Icon type="delete" /></span>
                                                                    </Popconfirm>
                                                                </td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Dialog header="Cadastrar Nova URL" footer={footerModalCreate} visible={this.state.isModalCreate} style={{ width: '50vw' }} modal={true}
                        onHide={this.onHideModalCreate}>
                        <div style={{ paddingTop: "20px" }}>
                            <span className="p-float-label">
                                <InputText id="urlModal" type="text" size="100" value={this.state.urlParam}
                                    onChange={(e) => this.setState({ urlParam: e.target.value })} />
                                <label htmlFor="urlModal">URL</label>
                            </span>
                        </div>
                    </Dialog>

                    <Dialog header="Remover URL" footer={footerModalDelete} visible={this.state.isModalDelete} style={{ width: '50vw' }} modal={true}
                        onHide={() => this.setState({ isModalDelete: false })}>
                        Tem certeza que deseja excluir a URL?
                </Dialog>

                </div>
            );
        }
    })
