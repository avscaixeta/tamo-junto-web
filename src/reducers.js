import { combineReducers, applyMiddleware } from 'redux';
import newsReducer from './components/news/NewsReducer';

const rootReducer = 
    combineReducers({
        newsReducer,
    })

export default rootReducer;