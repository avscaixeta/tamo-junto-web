import React, { Component } from 'react';

class ProgressBar extends Component {
    
    render() {
        return (
            <div className={this.props.className} style={{width: '100%', height:'24px', backgroundColor: '#EDEBEE', position:'relative'}}>
                <div style={{width: (this.props.percentage || 0)+'%', height:'100%', backgroundColor: this.props.color || '#77CCDD'}}></div>
                <div style={{position:'absolute', top:'4px', left:'8px', fontSize:'12px'}}>{this.props.label}</div>
                <div style={{position:'absolute', top:'4px', right:'8px', fontSize:'12px', fontWeight:'bold'}}>{this.props.percentage || 0}%</div>
            </div>
        );
    }
}


export default ProgressBar;