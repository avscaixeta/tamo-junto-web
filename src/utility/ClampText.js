export default function ClampText(text, maxLength){
    text = (text || "").toString();
    maxLength = maxLength || 30;

    if(text.length > maxLength){
        text = text.substr(0,maxLength).trim() + "...";
    }

    return text;
};