import React, { Component } from 'react';
import { Route, withRouter } from "react-router-dom";
import AdaptativaHeader from '../commons/AdaptativaHeader';
import Simulado from './simulado/Simulado';
import Acompanhamento from './acompanhamento/Acompanhamento';

class CoordenadorPage extends Component {

    getActiveTabColor(){
        var path = this.props.location.pathname;

        if(path === "/coord/acompanhamento"){ return " adap-border-yellow"; }
        if(path === "/coord/simulado"){ return " adap-border-red"; }
        if(path === "/coord/biblioteca"){ return " adap-border-blue"; }
        if(path === "/coord/correcao"){ return " adap-border-green"; }
        
        //Se não está em nenhuma das rotas anteriores, utiliza a cor da aba inicial
        return " adap-border-red";
    }
    render() {
        return (
            <div className="edb-padding-large-left edb-padding-large-right">
                <AdaptativaHeader/>
                
                {/* Navegação das Abas */}
                <div className={"edb-margin-top edb-bottombar " + this.getActiveTabColor()}>
                    <a className="edb-btn adap-yellow" href="#/coord/acompanhamento">&nbsp;Acompanhamento&nbsp;</a>
                    <a className="edb-btn adap-red" href="#/coord/simulado">&nbsp;Simulados&nbsp;</a>
                    <a className="edb-btn adap-blue" href="#/coord/biblioteca">Biblioteca de Conteúdo</a>
                    <a className="edb-btn adap-green" href="#/coord/correcao">Correção de atividades <i className="fas fa-exclamation-triangle"></i></a>
                </div>                 

                {/* Aba padrão */}
                <Route exact path="/coord" component={Simulado} />

                {/* Abas */}
                <Route path="/coord/simulado" component={Simulado} />
                <Route path="/coord/acompanhamento" component={Acompanhamento} />
            </div>
        );
    }
}

export default withRouter(props => <CoordenadorPage {...props}/>);