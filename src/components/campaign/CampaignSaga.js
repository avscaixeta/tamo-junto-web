import { put, call } from 'redux-saga/effects';
import axios from 'axios';

const URL = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/rest/v1/campaign' : 'https://tamojuntoweb.herokuapp.com/rest/v1/campaign';

export function fetchAllCampaignFromApi() {
    return axios.get(`${URL}`)
}

export function* fetchAllDocuments() {
    console.log('Buscando os documentos....')
    try {
        const response = yield call(fetchAllCampaignFromApi);
        yield put({ type: 'SUCCESS_FIND_ALL', payload: {data: response } })
    } catch (err){
        yield put({ type: 'FAILURE_FIND_ALL' })
    }
}

export function* saveNewDocument(formData) {
    console.log('Salvando o arquivo...')
    for (var key of formData.data.entries()) {
        console.log(key[0] + ', ' + key[1]);
    }
    try {
        yield call(axios.post, URL, formData.data, 
        {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
        yield put({ type: 'SUCCESS_CREATE_DOCUMENT' })
    } catch (err) {
        yield put({ type: 'FAILURE_CREATE_DOCUMENT' })
    }
}