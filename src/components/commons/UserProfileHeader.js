import React, { Component } from 'react';

class UserProfileHeader extends Component {
    render() {
        return (
            <div className={this.props.className + ' adap-text-darkgray edb-padding-bottom'}>

                <div className="edb-row">
                    <div className="edb-col s3 edb-center edb-padding-small">
                        <img className="edb-image" width="88" src="/images/perfil2.png" alt="Foto do perfil" />
                    </div>
                    <div className="edb-col s9 edb-container edb-cell-middle">
                        <h6 className="edb-margin-none"><b>Laura Ferreira Nunes</b></h6>
                        <div className="edb-small">Aluna Ensino Médio - EM1B</div>
                        <div className="edb-small">ISJB - Colegio Dom Bosco - Araxa</div>

                        <div className="edb-row  edb-margin-small-top">
                            <div className="edb-col s4">
                                <div className="edb-tiny">NOTA TRI.</div>
                                <div className="edb-text-bold edb-large">760</div>
                            </div>
                            <div className="edb-col s4">
                                <div className="edb-tiny">PONTOS</div>
                                <div className="edb-text-bold edb-large">120</div>
                            </div>
                            <div className="edb-col s4">
                                <div className="edb-tiny">COLOCAÇÂO</div>
                                <div className="edb-text-bold edb-large">875º</div>
                            </div>
                        </div>
                    </div>
                </div>





            </div>
        );
    }
}

export default UserProfileHeader;