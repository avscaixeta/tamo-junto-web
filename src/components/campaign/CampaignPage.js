import React, { Component } from 'react';
import { connect } from 'react-redux';
import AdaptativaHeader from '../commons/AdaptativaHeader';
import { bindActionCreators } from 'redux';
import { findAllDocuments, saveNewDocument } from './CampaignActions';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { Icon, message } from 'antd';

import "./CampaignPage.css"

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispath => {
    return bindActionCreators({ findAllDocuments, saveNewDocument }, dispath);
};

export default connect(mapStateToProps, mapDispatchToProps)(
    class CampaignPage extends Component {

        constructor() {
            super();
            this.state = {
                selectedFile: null,
                files: [],
                loading: false,
            }
        }

        uploadHandler = event => {
            console.log(event.files)
            //returns Promise object
            this.setState({
                selectedFile: event.files[0]
            });
            const formData = new FormData();
            formData.append('file', this.state.selectedFile);
            this.props.saveNewDocument(formData)
        }

        getBase64(img, callback) {
            const reader = new FileReader();
            reader.addEventListener('load', () => callback(reader.result));
            reader.readAsDataURL(img);
        }

        beforeUpload(file) {
            // const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
            // if (!isJpgOrPng) {
            //     message.error('You can only upload JPG/PNG file!');
            // }
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (!isLt2M) {
                message.error('Image must smaller than 2MB!');
            }
            return isLt2M;
        }

        onChangeHandler=event=>{
            console.log(event.target.files[0])
            this.setState({
              selectedFile: event.target.files[0],
              loading: false,
            })
          }

        onClickHandler = () => {
            const data = new FormData() 
            data.append('file', this.state.selectedFile)
            this.props.saveNewDocument(data)
        }

        render() {
            return (
                <div className="edb-padding-large-left edb-padding-large-right">
                    <AdaptativaHeader />
                    <div>
                        <div className="row">
                            <div className="col-lg-12" style={{ paddingTop: "10px", paddingBottom: "10px" }}>
                                <span><h2>Gerenciamento da Campanha</h2></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <Card title="SEGURANÇA" subTitle="Fazer upload dos arquivos referentes a Segurança">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group files color">
                                                <input type="file" className="form-control" multiple="" name="file" onChange={this.onChangeHandler} />
                                            </div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <Card title="EDUCAÇÃO" subTitle="Fazer upload dos arquivos referentes a Educação">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group files color">
                                                <input type="file" className="form-control" multiple="" name="file" onChange={this.onChangeHandler} />
                                            </div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <Card title="INFRAESTRUTURA" subTitle="Fazer upload dos arquivos referentes as propostas de Infraestrutura">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group files color">
                                                <input type="file" className="form-control" multiple="" name="file" onChange={this.onChangeHandler} />
                                            </div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <Card title="CONVITES/CHAMADAS" subTitle="Fazer upload dos arquivos referentes a convites e chamadas">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group files color">
                                                <input type="file" className="form-control" multiple="" name="file" onChange={this.onChangeHandler} />
                                            </div>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-right" style={{
                                "paddingTop": "20px",
                                "paddingBottom": "50px",
                                "paddingRight": "30px"
                            }}>
                                <Button label="Salvar" className="btn btn-success" onClick={this.onClickHandler} />
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid container-fixed-sm">
                        <div className="copyright sm-text-center">
                            <p className="small no-margin pull-left sm-pull-reset">
                                <span>Copyright </span>
                                <span>CONTRAFLUXO</span>
                                <span>Todos os direitos reservados.</span>
                            </p>
                        </div>
                    </div>
                </div>
            );
        }
    })