import React, { Component } from 'react';
import Rate from 'rc-rate';
import '../../../styles/rc-rate.css';
import GaugeChartD3 from '../../charts/GaugeChartD3';


class CardLivrosRecentes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showEmptyState: false,
            livros: [
                {img: "/images/livros/livro-geografia.jpg", titulo:'Geografia 1'},
                {img: "/images/livros/livro-fisica.jpg", titulo:'Física 1'},
                {img: "/images/livros/livro-matematica.jpg", titulo:'Matemática 1'},
            ]
        };
    }
    render() {
        return (
            <div className="edb-card adap-card adap-card-livros edb-topbar adap-border-red">
                <header className="edb-container edb-margin-small-top">
                    <span className="edb-large edb-text-bolder">LIVROS RECENTES</span>
                    <div className="edb-display-right edb-margin-right">
                        <Rate count="3" character={<i className="fas fa-star"></i>}/>
                        <i className="fas fa-ellipsis-h edb-margin-left"></i>
                    </div>
                </header>
                {this.state.showEmptyState &&
                    <div className="edb-container edb-padding-bottom">

                        <div className="edb-cell-row edb-padding-top">

                            <div className="edb-container edb-cell">
                                <span className="edb-xxlarge">''</span>
                            </div>

                            <div className="edb-cell edb-cell-middle">
                                <p>Meus filhos terão computadores, sim, mas antes terão livros. Sem livros, sem leitura, os nossos filhos serão incapazes de escrever inclusive a sua própria história.</p>
                            </div>

                            <div className="edb-container edb-cell edb-cell-bottom">
                                <div className="edb-xxlarge edb-padding-top" style={{ lineHeight: "24px" }}>''</div>
                            </div>
                        </div>

                        <div className="edb-container edb-right-align">Bill Gates</div>
                    </div>
                }
                {!this.state.showEmptyState &&
                    <div className="edb-container edb-padding-bottom">

                        <div className="adap-scroll-horizontal edb-margin-small-top" >
                            {this.state.livros.map(item =>
                                <img className="edb-margin-right" src={item.img} alt={item.titulo} />
                            )}
                        </div>
                        <div className="edb-center edb-margin-top edb-hide-medium edb-hide-large">
                                <i className="fas fa-plus"></i> <span className="edb-small"> Ver tudo</span>
                        </div>
                                <div className="edb-small  edb-hide-small">ACOMPANHAMENTO PEDAGÓGICO</div>
                        <div className="edb-row edb-margin-small-top edb-hide-small">
                            <div className="edb-col s6 m4">

                                <div>
                                    <i className="fas fa-arrow-circle-up adap-text-green edb-margin-small-right"></i><small>Matemática</small>
                                </div>
                                <div>
                                    <i className="fas fa-minus-circle adap-text-yellow edb-margin-small-right"></i><small>Biologia</small>
                                </div>
                                <div>
                                    <i className="fas fa-arrow-circle-down adap-text-red edb-margin-small-right"></i><small>História</small>
                                </div>
                            </div>
                            <div className="edb-col s6 m4">
                                <GaugeChartD3/>
                            </div>
                            <div className="edb-col s12 m4 edb-center">
                                <div className="edb-large">LEIA+</div>
                                <div className="edb-small">475 LIVROS <br/> GRATUITOS</div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default CardLivrosRecentes;